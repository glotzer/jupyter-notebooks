# Collection of public jupyter notebooks for the Glotzer Group.

* Maintainer: Carl Simon Adorf
* LICENSE: All source code published in this repository is licensed under the MIT license, see `LICENSE.txt`.

## How to display the notebooks with nbviewer

This is a public repository, which means that you can use [nbviewer](http://nbviewer.jupyter.org/) to view the notebooks.

### Using a chrome add-on

  1. Install the [Bitbucket NBViewer Button
](https://chrome.google.com/webstore/detail/bitbucket-nbviewer-button/pmffdaflalienoekibbmcblmabgkcoac).
  2. Open the [notebook files](https://bitbucket.org/glotzer/jupyter-notebooks/src) within this repository.
  3. Click on the `NBView` button to view the notebook online.

### Manually 

Copy and paste the raw source url, e.g. for [signac-flow](https://bitbucket.org/glotzer/jupyter-notebooks/raw/master/signac-flow.ipynb), into the URL field of [nbviewer](http://nbviewer.jupyter.org/).

## Notebooks within this repository

  * [signac-flow](https://bitbucket.org/glotzer/jupyter-notebooks/raw/master/signac-flow.ipynb): Demonstration on how to setup a signac-flow project from scratch.
  * [signac-flow-hoomd-blue](https://bitbucket.org/glotzer/jupyter-notebooks/raw/master/signac-flow-hoomd-blue.ipynb): Demonstration on how to setup a signac-flow project from scratch in combination with HOOMD-blue.